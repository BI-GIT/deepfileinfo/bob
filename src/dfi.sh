ls -la "$1" || exit

filetype=$(file -b "$1")
printf "Type:\t%s\n" "$filetype"

if command -v exiftool &>/dev/null; then
  out=$(exiftool "$1")
  if [[ $? -eq 0 ]]; then
    echo "exif:"
    echo "$out"
  fi
fi

if [[ $filetype =~ "ISO Media" ]]; then
  if command -v ffprobe &>/dev/null; then
    echo "ffprobe:"
    ffprobe -hide_banner "$1"
  else
    echo "For more information, install ffprobe"
  fi
fi

if [[ $filetype =~ "vCalendar" ]]; then
  if command -v dfi_ical_peek &>/dev/null; then
    dfi_ical_peek "$1"
  else
    echo "Incomplete installation, run make install"
  fi
fi
